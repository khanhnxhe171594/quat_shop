/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

/**
 *
 * @author Nitro
 */
public class Size {

    private Products product;
    private String size;

    public Size() {
    }

    public Size(Products product, String size) {
        this.product = product;
        this.size = size;
    }



    public Products getProduct() {
        return product;
    }

    public void setProduct(Products product) {
        this.product = product;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "Size{" + "product=" + product + ", size=" + size + '}';
    }
    
    
    
    
}
