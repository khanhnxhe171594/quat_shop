/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package models;

import java.util.ArrayList;
import java.util.List;
import models.Item;
import models.Products;

public class Cart {

    private List<Item> items;

    public Cart() {
        items = new ArrayList<>();
    }

    public List<Item> getItems() {
        return items;
    }

    public int getQuantityById(int id) {
        return getItemById(id).getQuantity();
    }

    private Item getItemById(int id) {
        for (Item i : items) {
            if (i.getProduct().getProductID() == id) {
                return i;
            }
        }
        return null;
    }

    public void addItem(Item t) {
        if (getItemById(t.getProduct().getProductID()) != null) {
            Item m = getItemById(t.getProduct().getProductID());
            m.setQuantity(m.getQuantity() + t.getQuantity());
        } else {
            items.add(t);
        }
    }

    public void removeItem(int id) {
        if (getItemById(id) != null) {
            items.remove(getItemById(id));
        }
    }

    public double getTotalMoney() {
        double t = 0;
        for (Item i : items) {
            t += (i.getQuantity() * i.getPrice() );
        }
        return t;
    }

    private Products getProductById(int pid, List<Products> list) {
        for (Products i : list) {
            if (i.getProductID() == pid) {
                return i;
            }
        }
        return null;
    }

    private Size getSizeById(int pid, List<Size> list) {
        for (Size i : list) {
            if (i.getProduct().getProductID() == pid) {
                return i;
            }
        }
        return null;
    }

    public Cart(String txt, List<Products> list, List<Size> list1) {
        items = new ArrayList<>();
        try {
            if (txt != null && txt.length() != 0 && list != null) {
                String[] s = txt.split("/");
                for (String i : s) {
                    String[] n = i.split(":");
                    if (n.length >= 2) {
                        int pid = Integer.parseInt(n[0]);
                        int quantity = Integer.parseInt(n[1]);
                        Size h = getSizeById(pid, list1);

                        Products p = getProductById(pid, list);
                        if (p != null) {
                            Item t = new Item(p, h, quantity, (p.getPrice() * (100.0 - p.getDiscountSale() ) / 100.0));
                            addItem(t);
                        } else {
                            // Handle the case where no matching product is found
                        }
                    } else {
                        // Handle the case where the format of txt is incorrect
                    }
                }
            }
        } catch (NumberFormatException e) {
            // Handle the case where parsing integers from txt fails
        }
    }

}
